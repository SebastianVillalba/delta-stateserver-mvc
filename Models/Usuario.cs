﻿using System;

namespace AppPoc.Models
{
    [Serializable]
    public class Usuario
    {
        public string Nombre { get; set; }
        public string Apellido { get; set; }
    }
}