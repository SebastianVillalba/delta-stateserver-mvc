#----------------------------------------------------------------------------------INSTANCIA------------------------------
#
FROM mcr.microsoft.com/dotnet/framework/aspnet:4.8-windowsservercore-ltsc2019
ARG source
WORKDIR /inetpub/wwwroot
COPY ${source:-obj/Docker/publish} .

#----------------------------------------------------------------------------------INSTANCIA------------------------------


#----------------------------------------------------------------------------------STATE SERVER------------------------------
#FROM mcr.microsoft.com/dotnet/framework/aspnet:4.8-windowsservercore-ltsc2019
#SHELL ["powershell", "-command"]
#EXPOSE 42424
#
##Activa Administración remota de IIS
##
#RUN Install-WindowsFeature Web-Mgmt-Service; \
#New-ItemProperty -Path HKLM:\software\microsoft\WebManagement\Server -Name EnableRemoteManagement -Value 1 -Force; \
#Set-Service -Name wmsvc -StartupType automatic;
#
#RUN Set-ItemProperty -Path HKLM:\SYSTEM\CurrentControlSet\Services\aspnet_state\Parameters -Name AllowRemoteConnection -Value 1
#
#RUN Set-Service -Name aspnet_state -StartupType automatic;
#RUN Start-Service -Name aspnet_state;
#
##Agrega usuario remoto
#
#RUN net user iisadmin Password~1234 /ADD; \
#net localgroup administrators iisadmin /add;
#----------------------------------------------------------------------------------STATE SERVER------------------------------
