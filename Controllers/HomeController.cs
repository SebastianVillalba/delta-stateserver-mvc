﻿using AppPoc.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace AppPoc.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult About()
        {
            ViewBag.Message = $"Hola {SesionPersona?.Nombre} {SesionPersona?.Apellido}";

            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }

        [HttpPost]
        public ActionResult Index(Usuario model)
        {
            SesionPersona = model;
            return View();
        }

        public Usuario SesionPersona
        {
            get
            {
                return Session["usuario"] as Usuario;
            }

            set
            {
                Session["usuario"] = value;
            }
        }
    }
}